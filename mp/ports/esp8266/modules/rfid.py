from machine import UART
from time import sleep
import uos
import sonoff

'''
Helper functions for communicating with the B1 RFID module
'''


def configure():
    uart0 = UART(0, 9600, bits=8, parity=None, stop=1,
                 timeout=1000, timeout_char=10)
    uart1 = UART(1, 115200)

    # Configure polling settings
    uart0.write('\x02\x87\x00\xF3\x20\x01\x58\x02\x80\x00\x22\x01\x00\x02\x00\x03\x00\x00\x00\x00\x05\x02\x00\x03\x00\x00\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xEA\x3C')
    uart0.read(8)
    uart0.write('\x02\x08\x00\x55\x2B\x01\x01\x00\x01\x00\x1C\x6C\x05')
    uart0.read(17)

    # Start Polling
    uart0.write('\x02\x1A\x00\x44\x4E\x01\x01\x00\x13\x00\x22\x01\x00\x02\x00\x03\x00\x00\x00\x00\x05\x02\x00\x03\x00\x00\x00\x00\x05\x05\xCA')
    uart0.read(17)
    uart0.write('\x02\x07\x00\x6B\x3B\x02\x00\x00\x01\x00\xBE\x66')
    uart0.read(14)


def poll():
    import ubinascii

    uos.dupterm(None, 1)
    uart0 = UART(0, 9600, bits=8, parity=None, stop=1,
                 timeout=1000, timeout_char=10)
    uart1 = UART(1, 115200)

    while(True):
        data = uart0.read(5)
        if data:
            uart1.write(str(data))
            sonoff.led(1)
            sonoff.relay(1)
        else:
            uart1.write('-')
            sonoff.led(0)
            sonoff.relay(1)


def test():
    poll()
    UART(1).write('No more poll')

    # Begin duplicating stream to REPL
    uos.dupterm(UART(0, 115200), 1)


def uid():
    uos.dupterm(None, 1)
    uart0 = UART(0, 9600, bits=8, parity=None, stop=1,
                 timeout=1000, timeout_char=10)
    data = uart0.read(5)
    if data:
        uid = int(hex(int.from_bytes(data, 'little'))[2:10], 16)
        return uid
    else:
        return None
