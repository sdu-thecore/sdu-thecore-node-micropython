from machine import Pin


def led(state):
    pin = Pin(13, Pin.OUT)
    # The output logic is reversed
    if state:
        pin.off()
    else:
        pin.on()


def relay(state):
    pin = Pin(12, Pin.OUT)

    if(state):
        pin.on()
    else:
        pin.off()
