import time
import sonoff
import neopixelFunc as neo
import networkFunc
import network
import rfid
import wifiRecovery

sta = network.WLAN(network.STA_IF)
np = neo.init()

sonoff.led(1)  # Turn on green LED to indicate powered state
neo.maintenance(np)  # Set indicator LED to maintainance during startup
time.sleep(5)  # Small delay to allow wifi to connect

if not sta.isconnected():
    neo.blink(np, 'orange', 2)
    wifiRecovery.recover()

nodeId = None
while nodeId == None:
    nodeId = networkFunc.createNode()  # Try to create node, returns Node ID

sessionId = None
token = None
while sessionId == None:
    sessionId = networkFunc.createSession(nodeId)  # Create a new session

# Wait for session to be authenticated
sessionAuth = False
while not sessionAuth:
    neo.maintenance(np)
    sessionAuth, identify = networkFunc.checkSession(sessionId, nodeId)
    # If identify is checked, blink led
    if identify:
        neo.identify(np)
    else:
        time.sleep(1.0)

# Wait for machine to be assigned
machineId = None
while machineId == None:
    machineId = networkFunc.getMachine(nodeId)

scheduling = None
workflow = None
permissionId = None
while scheduling is None or workflow is None:
    scheduling, workflow, permissionId = networkFunc.getProperties(machineId)

networkFunc.setStatus(machineId, 'ready')

tag = None
if scheduling == 'on_demand':
    neo.setColor(np, 'WHITE')  # TESTING
    time.sleep(0.5)  # TESTING
    if workflow == 'open':
        neo.setColor(np, 'YELLOW')  # TESTING
        time.sleep(0.5)  # TESTING
        while True:  # Main card detection loop
            while tag == None:
                sonoff.relay(0)
                neo.blink(np, "VIOLET")  # TESTING
                tag = rfid.uid()

            if networkFunc.checkAccess(tag, permissionId):
                neo.approved(np)
                sonoff.relay(1)
            else:
                neo.denied(np)
                sonoff.relay(0)

            prevTag = tag
            while tag == prevTag:
                tag = rfid.uid()
                neo.blink(np, 'BLUE')

            sonoff.relay(0)
            neo.blink(np, 'WHITE')
else:
    neo.blink(np, 'WHITE')  # TESTING
